<?php
function custom_add_termandcondition_endpoint() {
   add_rewrite_endpoint( 'termandcondition', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'custom_add_termandcondition_endpoint' );
 
 
/**
* 2. Add new query var
*/
function custom_termandcondition_query_vars( $vars ) {
   $vars[] = 'termandcondition';
   return $vars;
}
 
add_filter( 'woocommerce_get_query_vars', 'custom_termandcondition_query_vars', 0 );
 
 
/**
* 3. Insert the new endpoint into the My Account menu
*/
function custom_add_termandcondition_link_my_account( $items ) {
   $items['termandcondition'] = __('เงื่อนไขและบริการลูกค้า', 'xstore');
   return $items;
}
 
add_filter( 'woocommerce_account_menu_items', 'custom_add_termandcondition_link_my_account' );
 
 
/**
* 4. Add content to the new endpoint
*/
function custom_termandcondition_content() {
   $args = [
      'post_type' => 'staticblocks',
      'postname' => 'term-and-condition',
   ];
   $the_query = new WP_Query( $args );
   if ($the_query->have_posts()) {
     
      while ($the_query->have_posts()) {
         $the_query->the_post();
         get_template_part( 'woocommerce/myaccount/custom', 'termandcondition');
         break;
      }
   }
   $the_query->reset_postdata();
}

/**
* @important-note	"add_action" must follow 'woocommerce_account_{your-endpoint-slug}_endpoint' format
*/
add_action( 'woocommerce_account_termandcondition_endpoint', 'custom_termandcondition_content' );