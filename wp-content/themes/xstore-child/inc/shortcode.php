<?php
function girl_and_boy_top_nav_btn_shortcode( $atts ){
    ob_start();
    ?>
    <div id="boyTopNavBtn" data-trigger="boy">&nbsp;</div>
    <div id="girlTopNavBtn" data-trigger="girl">&nbsp;</div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#girlTopNavBtn, #boyTopNavBtn').on('click', function(){
                var target = $(this).data('trigger');
                if ($('.girl-boy-dropdown-menu[data-target="'+target+'"]').is(':visible')) {
                    $('.girl-boy-dropdown-menu[data-target="'+target+'"]').fadeOut();
                } else {
                    $('.girl-boy-dropdown-menu').fadeOut();
                    $('.girl-boy-dropdown-menu[data-target="'+target+'"]').fadeIn();
                }
            });

            $('#girlTopNavBtn, #boyTopNavBtn').mouseover(function() {
                var target = $(this).data('trigger');
                if (!$('.girl-boy-dropdown-menu[data-target="'+target+'"]').is(':visible')) {
                    $('.girl-boy-dropdown-menu').fadeOut();
                    $('.girl-boy-dropdown-menu[data-target="'+target+'"]').fadeIn();
                }
            });

            $('.girl-boy-dropdown-menu').mouseleave(function() {
               $('.girl-boy-dropdown-menu').fadeOut();
            });

            $('body').on('keyup', function(e){
                // press ESC button on keyboard
                if(e.which == 27){
                    // close dropdown menu
                    $('.girl-boy-dropdown-menu').fadeOut();

                    // close mobile menu
                    $('#custom-mobile-menu-content').removeClass('open');
                    $('body').removeClass('mobilemenu-open');
                }
            });
        });
    </script>
    <?php
    $output = ob_get_clean();
    add_action('cmprodev_add_dropdown_menu', function() use ($atts){
        ob_start();
        ?>
        <div class="girl-boy-dropdown-menu" data-target="girl">
             <?php
             $girlCategoryId = $atts['girlcateid'];
             $catTerms = get_terms('product_cat', array(
                  'hide_empty' => 0,  
                  'order' => 'ASC', 
                  'child_of'=> $girlCategoryId
             ));
             ?>
             <div class="container">
                  <h3 class="parent-category-title"><?php echo get_term( $girlCategoryId )->name; ?></h3>
                  <div class="row">
                  <?php foreach($catTerms as $catTerm) : ?>
                  <?php $thumbnail_id = get_term_meta( $catTerm->term_id, 'thumbnail_id', true ); 
                  $image = wp_get_attachment_url( $thumbnail_id );  ?>
                       <div class="col-xs-6 col-sm-4 col-md-3">
                       <a href="<?php echo get_term_link($catTerm->term_id) ?>">
                            <img src="<?php echo $image; ?>" alt=""/>
                            <div class="category-title"><?php echo $catTerm->name; ?></div>
                       </a>
                       </div>
                  <?php endforeach; ?>
                  </div>
             </div>
        </div>

        <div class="girl-boy-dropdown-menu" data-target="boy">
             <?php
             $boyCategoryId = $atts['boycateid'];
             $catTerms = get_terms('product_cat', array(
                  'hide_empty' => 0,  
                  'order' => 'ASC', 
                  'child_of'=> $boyCategoryId
             ));
             ?>
             <div class="container">
                  <h3 class="parent-category-title"><?php echo get_term( $boyCategoryId )->name; ?></h3>
                  <div class="row">
                  <?php foreach($catTerms as $catTerm) : ?>
                  <?php $thumbnail_id = get_term_meta( $catTerm->term_id, 'thumbnail_id', true ); 
                  $image = wp_get_attachment_url( $thumbnail_id );  ?>
                       <div class="col-xs-6 col-sm-4 col-md-3">
                       <a href="<?php echo get_term_link($catTerm->term_id) ?>">
                            <img src="<?php echo $image; ?>" alt=""/>
                            <div class="category-title"><?php echo $catTerm->name; ?></div>
                       </a>
                       </div>
                  <?php endforeach; ?>
                  </div>
             </div>
        </div>
        <?php echo ob_get_clean();
    });
    return $output;
}
add_shortcode('girl_and_boy_top_nav_btn', 'girl_and_boy_top_nav_btn_shortcode');


add_shortcode('cmprodev_mobile_menu', 'cmprodev_mobile_menu');
function cmprodev_mobile_menu() {
     ob_start(); ?>
     <?php
     $profileImgUrl = get_stylesheet_directory_uri().'/images/guest-profile.svg';
     $user = wp_get_current_user();
     if ($user->ID) {
          $profileImgUrl = get_avatar_url( $user->ID );
     }
     $boyCatLink = get_term_link('boy-collection', 'product_cat');
     $girlCatLink = get_term_link('girl-collection', 'product_cat');
     $aboutUsLink = get_permalink(get_page_by_path('about-us'));
     $userDisplayName = wp_get_current_user()->ID ? wp_get_current_user()->display_name : __('Guest', 'xstore');
     $isGuest = $user->ID ? false : true;
     $wishlistLink = get_permalink(get_page_by_path('wishlist'));
     ?>

     <div id="custom-mobile-menu">&nbsp;</div>
     <div id="custom-mobile-menu-content">
                    <div class="profile-pic">
                         <img src="<?php echo esc_url($profileImgUrl); ?>" alt="">
                    </div>
                    <div class="hello-user d-md-hide"> <?php echo esc_html_e('Hello '.$userDisplayName) ?> </div>
               <?php if ($isGuest): ?>
                    <ul class="menu-wrapper d-md-hide">
                         <li>
                              <a href="<?php echo get_permalink(8329); ?>"><span class="register-icon"></span> <?php echo esc_html_e('Register') ?></a>
                         </li>
                         <li>
                              <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><span class="login-icon"></span> <?php echo esc_html_e('Login') ?></a>
                         </li>
                         <li>
                              <a class="cart-customlocation" href="<?php echo wc_get_checkout_url() ?>"><span class="cart-icon"></span> <?php echo esc_html_e('Your cart') ?></a>
                         </li>
                    </ul>
               <?php endif; ?>

                    <?php if ($isGuest === false): ?>
                         <ul class="menu-wrapper d-md-hide">
                              <li>
                                   <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><span class="myacc-icon"></span> <?php echo esc_html_e('My Account') ?></a>
                              </li>
                              <li>
                                   <a class="cart-customlocation" href="<?php echo wc_get_checkout_url() ?>"><span class="cart-icon"></span> <?php echo esc_html_e('Your cart') ?></a>
                              </li>
                              <li>
                                   <a href="<?php echo $wishlistLink ?>"><span class="wishlist-icon"></span> <?php echo esc_html_e('Your Wishlist') ?></a>
                              </li>
                              <li>
                                   <a href="<?php echo wp_logout_url( get_permalink() ); ?>"><span class="logout-icon"></span> <?php echo esc_html_e('Log out') ?></a>
                              </li>
                         </ul>
                    <?php endif; ?>

          <div class="custom-mobile-menu-content-slide">
               <div class="top-menu-mobile-tools">
                    <span class="close-menu-mobile">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24.09" height="24.09" viewBox="0 0 24.09 24.09">
  <g id="Group_2613" data-name="Group 2613" transform="translate(-73 -10)">
    <rect id="Rectangle_197" data-name="Rectangle 197" width="30" height="4.068" transform="translate(75.877 10) rotate(45)" fill="#fff"/>
    <rect id="Rectangle_1495" data-name="Rectangle 1495" width="30" height="4.068" transform="translate(73 31.213) rotate(-45)" fill="#fff"/>
  </g>
</svg>
                    </span>
                    <span class="back-menu-slide"><svg xmlns="http://www.w3.org/2000/svg" width="13" height="20.033" viewBox="0 0 13 20.033">
                    <g id="_83" data-name=" 83" transform="translate(715 -683) rotate(90)">
                    <path id="Path_36" data-name="Path 36" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
                    <path id="Path_2236" data-name="Path 2236" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
                    </g>
                    </svg><?php echo esc_html_e('Back') ?></span>
               </div>
               <div class="clearfix"></div>
               <div class="menu-content-slide">
                    <div id="mobile-search">
                         <?php dynamic_sidebar( 'mobile_menu_top' ); ?> 
                    </div>
                    <ul class="menu-wrapper">
                         <li>
                              <a href="<?php echo $boyCatLink ?>"><span class="boy-category-icon"></span> <?php echo esc_html_e('Boy Category') ?>
                              <svg xmlns="http://www.w3.org/2000/svg" width="13" height="20.033" viewBox="0 0 13 20.033">
  <g id="_82" data-name=" 82" transform="translate(-702 703.033) rotate(-90)">
    <path id="Path_36" data-name="Path 36" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
    <path id="Path_2236" data-name="Path 2236" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
  </g>
</svg>

                              </a>
                              <?php woocommerce_subcats_from_parentcat_by_ID( 144 ); ?>
                         </li>
                         <li>
                              <a href="<?php echo $girlCatLink ?>"><span class="girl-category-icon"></span> <?php echo esc_html_e('Girl Category') ?>
                              <svg xmlns="http://www.w3.org/2000/svg" width="13" height="20.033" viewBox="0 0 13 20.033">
  <g id="_82" data-name=" 82" transform="translate(-702 703.033) rotate(-90)">
    <path id="Path_36" data-name="Path 36" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
    <path id="Path_2236" data-name="Path 2236" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
  </g>
</svg>

                              </a>
                              <?php woocommerce_subcats_from_parentcat_by_ID( 155 ); ?>
                         </li>
                         <li>
                              <a href=""><span class="blog-icon"></span> <?php echo esc_html_e('Blog and news') ?></a>
                         </li>
                         <li>
                              <a href="<?php echo get_permalink(get_page_by_path('faq')) ?>"><span class="faq-icon"></span> <?php echo esc_html_e('FAQ') ?></a>
                         </li>
                         <li>
                              <a href="<?php echo $aboutUsLink ?>"><span class="aboutus-icon"></span> <?php echo esc_html_e('About us') ?></a>
                         </li>
                         <li>
                              <a href="<?php echo get_permalink(get_page_by_path('contact-us')) ?>"><span class="contactus-icon"></span> <?php echo esc_html_e('Contact us') ?></a>
                         </li>
                         <li class="up-lg-hide lang-currency">
                         <?php 
                              if(function_exists('icl_get_languages')):
                                   $langs = icl_get_languages();
                              endif; 

                              if(!empty($langs)):
                                   $active_lang_flag_url = $langs[ICL_LANGUAGE_CODE]['country_flag_url'];
                                   $active_lang_flag_img = "<img class='wpml-cust-image-class' src='$active_lang_flag_url' />"; 
                              endif; 
                         ?>
                              <a href="#"><span class="lange-icon"><?php if(!empty($langs)){ echo $active_lang_flag_img;  }?></span> <?php echo esc_html_e('language & currency') ?>
                              <svg xmlns="http://www.w3.org/2000/svg" width="13" height="20.033" viewBox="0 0 13 20.033">
  <g id="_82" data-name=" 82" transform="translate(-702 703.033) rotate(-90)">
    <path id="Path_36" data-name="Path 36" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
    <path id="Path_2236" data-name="Path 2236" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
  </g>
</svg>
                         </a>
                              <div class="wooc_sclist">
                                   <?php dynamic_sidebar( 'lang_and_currency' ); ?> 
                              </div>
                         </li>
                    </ul>
                    <div class="menu-bottom-widget">
                         <?php dynamic_sidebar( 'mobile_menu_bottom' ); ?> 
                    </div>
               </div>
          </div>
          <div id="hide-hamburgermenu"></div>
     </div>
     <script type="text/javascript">
     $(document).ready(function(){
          var mobilemenuopen = false;
          $('#custom-mobile-menu').on('click',function(e){
               if ( $('#custom-mobile-menu-content').hasClass('open') ) {
                    //$('#custom-mobile-menu-content').removeClass('open');
                    $('body').removeClass('mobilemenu-open');
                    mobilemenuopen = false;
               } else {
                    $('#custom-mobile-menu-content').addClass('open');
                    $('body').addClass('mobilemenu-open');
                    mobilemenuopen = true;
               }
          });

          submenuMobile();

          $('#hide-hamburgermenu, .close-menu-mobile').on('click', function(e){
               clearMenu(); 
               $('#custom-mobile-menu-content').removeClass('open');
               $('body').removeClass('mobilemenu-open');
               mobilemenuopen = false;
          });

          // hide mobile menu when mouse is clicked at outside of mobile menu
          document.addEventListener('click',function(e){
               if ( mobilemenuopen === true && e.target.id != 'custom-mobile-menu' ) {
                    if ( e.target.id !== 'custom-mobile-menu-content' &&
                    $(e.target).parents('#custom-mobile-menu-content').attr('id') !== 'custom-mobile-menu-content'
                    ) {
                         $('#custom-mobile-menu-content').removeClass('open');
                         $('body').removeClass('mobilemenu-open');
                    }
               }
          });
     });

     function submenuMobile(){
          clearMenu();
          //('.menu-slide .menu-wrapper > li > a').each();
          $('.menu-content-slide .menu-wrapper > li > a').click(function(){
               var screenWidth = $(window).width();
               if (screenWidth < 992) {
                    $('.menu-content-slide .wooc_sclist').removeClass('show-on');
                    if($(this).next().hasClass('wooc_sclist')){
                         $(this).next().addClass('show-on');
                         $('.menu-content-slide').css('margin-left', '-100%');
                         return false;
                    }
               }
          });
          $('.back-menu-slide').click(function(){
               clearMenu();
          });
     }
     function clearMenu(){
          $('.menu-content-slide .wooc_sclist').removeClass('show-on');
          $('.menu-content-slide').css('margin-left', '0');
     }
     </script>
     <?php echo ob_get_clean();
}
function woocommerce_subcats_from_parentcat_by_ID($parent_cat_ID) {
     $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent_cat_ID,
        'taxonomy' => 'product_cat'
     );
   $subcats = get_categories($args);
     echo '<ul class="wooc_sclist">';
       foreach ($subcats as $sc) {
            //print_r($sc);
          $thumbnail_id = get_term_meta( $sc->term_id, 'thumbnail_id', true ); 
          $image = wp_get_attachment_url( $thumbnail_id ); 
          $link = get_term_link( $sc->slug, $sc->taxonomy );
          echo '<li><a href="'. $link .'"> <img src="'.$image.'" alt="'.$sc->name.'" /> '.$sc->name.'</a></li>';
       }
     echo '</ul>';
 }

 add_shortcode('cmprodev_custom_mobile_profile_menu', 'cmprodev_custom_mobile_profile_menu');
 function cmprodev_custom_mobile_profile_menu(){
     ob_start(); ?>
     <?php
     $profileImgUrl = get_stylesheet_directory_uri().'/images/guest-profile.svg';
     $user = wp_get_current_user();
     if ($user->ID) {
          $profileImgUrl = get_avatar_url( $user->ID );
     }
     $boyCatLink = get_term_link('boy-collection', 'product_cat');
     $girlCatLink = get_term_link('girl-collection', 'product_cat');
     $aboutUsLink = get_permalink(get_page_by_path('about-us'));
     $userDisplayName = wp_get_current_user()->ID ? wp_get_current_user()->display_name : __('Guest', 'xstore');
     $isGuest = $user->ID ? false : true;
     $wishlistLink = get_permalink(get_page_by_path('wishlist'));
     ?>

     <div id="custom-mobile-profile-menu">
          <svg xmlns="http://www.w3.org/2000/svg" width="20.252" height="30" viewBox="0 0 20.252 30">
          <g id="Group_2521" data-name="Group 2521" transform="translate(3267.897 -554.744)">
          <path id="Path_788" data-name="Path 788" d="M-614.343,1669.022a.907.907,0,0,0,.907-.907v-3.26a3.974,3.974,0,0,1,3.969-3.969l.068,0,1.272,1.272a4.411,4.411,0,0,0,3.139,1.3,4.455,4.455,0,0,0,2.113-.535.907.907,0,0,0,.366-1.229.907.907,0,0,0-1.229-.366,2.634,2.634,0,0,1-1.25.317,2.609,2.609,0,0,1-1.857-.769l-.8-.8a2.442,2.442,0,0,0,.629-1.544,6.522,6.522,0,0,0,1.665.216h.378a6.582,6.582,0,0,0,6.574-6.574V1649.9a.907.907,0,0,0,.907.907.907.907,0,0,0,.907-.907v-3.382a7.406,7.406,0,0,0-7.291-7.5h-2.344a7.406,7.406,0,0,0-7.291,7.5v.964a.907.907,0,0,0,.907.907.907.907,0,0,0,.907-.907v-.964a5.591,5.591,0,0,1,5.478-5.686h2.344a5.591,5.591,0,0,1,5.478,5.686v2.022a.907.907,0,0,0-.745-.892,2.946,2.946,0,0,1-2.3-2.1.9.9,0,0,0-.676-.69.909.909,0,0,0-.917.324,11.569,11.569,0,0,1-5.81,3.872,12.019,12.019,0,0,1-3.219.547h-.01a.907.907,0,0,0-.906.9.907.907,0,0,0,.9.918,14.2,14.2,0,0,0,9.517-4.032,4.545,4.545,0,0,0,2.359,1.859v2.928a4.766,4.766,0,0,1-4.761,4.761h-.378a4.776,4.776,0,0,1-4.663-3.793.907.907,0,0,0-1.071-.7.907.907,0,0,0-.7,1.071,6.573,6.573,0,0,0,2.963,4.24v.687a.641.641,0,0,1-.64.64,5.79,5.79,0,0,0-5.783,5.783v3.26A.907.907,0,0,0-614.343,1669.022Z" transform="translate(-2652.647 -1084.278)" fill="#ef7a72"/>
          <path id="Path_789" data-name="Path 789" d="M-430,1914.3a.907.907,0,0,0,.907-.907v-3.26a5.789,5.789,0,0,0-5.783-5.783.907.907,0,0,0-.907.907.907.907,0,0,0,.907.907,3.974,3.974,0,0,1,3.969,3.97v3.26A.907.907,0,0,0-430,1914.3Z" transform="translate(-2818.557 -1329.552)" fill="#ef7a72"/>
          </g>
          </svg>
     </div>
     
     <div id="custom-mobile-profile-menu-content">
     <div class="top-profile-menu-mobile-tools">
                    <span class="close-profile-menu-mobile">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24.09" height="24.09" viewBox="0 0 24.09 24.09">
  <g id="Group_2613" data-name="Group 2613" transform="translate(-73 -10)">
    <rect id="Rectangle_197" data-name="Rectangle 197" width="30" height="4.068" transform="translate(75.877 10) rotate(45)" fill="#fff"/>
    <rect id="Rectangle_1495" data-name="Rectangle 1495" width="30" height="4.068" transform="translate(73 31.213) rotate(-45)" fill="#fff"/>
  </g>
</svg>
                    </span>
                    <span class="back-menu-slide"><svg xmlns="http://www.w3.org/2000/svg" width="13" height="20.033" viewBox="0 0 13 20.033">
                    <g id="_83" data-name=" 83" transform="translate(715 -683) rotate(90)">
                    <path id="Path_36" data-name="Path 36" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
                    <path id="Path_2236" data-name="Path 2236" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
                    </g>
                    </svg><?php echo esc_html_e('Back') ?></span>
               </div>
                    <div class="profile-pic">
                         <img src="<?php echo esc_url($profileImgUrl); ?>" alt="">
                    </div>
                    <div class="hello-user"> <?php echo esc_html_e('Hello '.$userDisplayName) ?> </div>
               <?php if ($isGuest): ?>
                    <ul class="menu-wrapper">
                         <li>
                              <a href="<?php echo get_permalink(8329); ?>"><span class="register-icon"></span> <?php echo esc_html_e('Register') ?></a>
                         </li>
                         <li>
                              <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><span class="login-icon"></span> <?php echo esc_html_e('Login') ?></a>
                         </li>
                    </ul>
               <?php endif; ?>

                    <?php if ($isGuest === false): ?>
                         <ul class="menu-wrapper">
                              <li>
                                   <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><span class="myacc-icon"></span> <?php echo esc_html_e('My Account') ?></a>
                              </li>
                              <li>
                                   <a class="cart-customlocation" href="<?php echo wc_get_checkout_url() ?>"><span class="cart-icon"></span> <?php echo esc_html_e('Your cart') ?></a>
                              </li>
                              <li>
                                   <a href="<?php echo $wishlistLink ?>"><span class="wishlist-icon"></span> <?php echo esc_html_e('Your Wishlist') ?></a>
                              </li>
                              <li>
                                   <a href="<?php echo wp_logout_url( get_permalink() ); ?>"><span class="logout-icon"></span> <?php echo esc_html_e('Log out') ?></a>
                              </li>
                         </ul>
                    <?php endif; ?>
     </div>
     <script type="text/javascript">
     $(document).ready(function(){
          var mobilemenuopen = false;
          $('#custom-mobile-profile-menu').on('click',function(e){
               if ( $('#custom-mobile-profile-menu-content').hasClass('open') ) {
                    //$('#custom-mobile-menu-content').removeClass('open');
                    $('body').removeClass('mobilemenu-open');
                    mobilemenuopen = false;
               } else {
                    $('#custom-mobile-profile-menu-content').addClass('open');
                    $('body').addClass('mobilemenu-open');
                    mobilemenuopen = true;
               }
          });

          $('.close-profile-menu-mobile').on('click', function(e){
               clearMenu(); 
               $('#custom-mobile-profile-menu-content').removeClass('open');
               $('body').removeClass('mobilemenu-open');
               mobilemenuopen = false;
          });
     });
     </script>
<?php
 }

add_shortcode('cmprodev_mobile_small_menu', 'cmprodev_mobile_small_menu');
function cmprodev_mobile_small_menu() {
     ob_start(); ?>
     <?php
     $profileImgUrl = get_stylesheet_directory_uri().'/images/guest-profile.svg';
     $user = wp_get_current_user();
     if ($user->ID) {
          $profileImgUrl = get_avatar_url( $user->ID );
     }
     $boyCatLink = get_term_link('boy-collection', 'product_cat');
     $girlCatLink = get_term_link('girl-collection', 'product_cat');
     $aboutUsLink = get_permalink(get_page_by_path('about-us'));
     $userDisplayName = wp_get_current_user()->ID ? wp_get_current_user()->display_name : __('Guest', 'xstore');
     $isGuest = $user->ID ? false : true;
     $wishlistLink = get_permalink(get_page_by_path('wishlist'));
     ?>

     <div id="custom-mobile-small-menu">&nbsp;</div>
     <div id="custom-mobile-small-menu-content">

          <ul class="menu-wrapper">
               <li>
                    <a href="<?php echo $boyCatLink ?>"><span class="boy-category-icon"></span> <?php echo esc_html_e('Boy Category') ?></a>
               </li>
               <li>
                    <a href="<?php echo $girlCatLink ?>"><span class="girl-category-icon"></span> <?php echo esc_html_e('Girl Category') ?></a>
               </li>
               <li>
                    <a href=""><span class="blog-icon"></span> <?php echo esc_html_e('Blog and news') ?></a>
               </li>
               <li>
                    <a href="<?php echo get_permalink(get_page_by_path('faq')) ?>"><span class="faq-icon"></span> <?php echo esc_html_e('FAQ') ?></a>
               </li>
               <li>
                    <a href="<?php echo $aboutUsLink ?>"><span class="aboutus-icon"></span> <?php echo esc_html_e('About us') ?></a>
               </li>
               <li>
                    <a href="<?php echo get_permalink(get_page_by_path('contact-us')) ?>"><span class="contactus-icon"></span> <?php echo esc_html_e('Contact us') ?></a>
               </li>
          </ul>

          <div id="hide-hamburgermenu-small"></div>
     </div>
     <script type="text/javascript">
     $(document).ready(function(){
          var mobilesmallmenuopen = false;
          $('#custom-mobile-small-menu').on('click',function(e){
               if ( $('#custom-mobile-small-menu-content').hasClass('open') ) {
                    //$('#custom-mobile-menu-content').removeClass('open');
                    $('body').removeClass('mobilemenu-small-open');
                    mobilesmallmenuopen = false;
               } else {
                    $('#custom-mobile-small-menu-content').addClass('open');
                    $('body').addClass('mobilemenu-small-open');
                    mobilesmallmenuopen = true;
               }
          });

          $('#hide-hamburgermenu-small').on('click', function(e){
               $('#custom-mobile-small-menu-content').removeClass('open');
               $('body').removeClass('mobilemenu-small-open');
               mobilesmallmenuopen = false;
          });

          // hide mobile menu when mouse is clicked at outside of mobile menu
          document.addEventListener('click',function(e){
               if ( mobilesmallmenuopen === true && e.target.id != 'custom-mobile-small-menu' ) {
                    if ( e.target.id !== 'custom-mobile-small-menu-content' &&
                    $(e.target).parents('#custom-mobile-small-menu-content').attr('id') !== 'custom-mobile-small-menu-content'
                    ) {
                         $('#custom-mobile-small-menu-content').removeClass('open');
                         $('body').removeClass('mobilemenu-small-open');
                    }
               }
          });
     });
     </script>
     <?php echo ob_get_clean();
}

add_shortcode('recent_post_grid', 'recent_post_grid_function');
function recent_post_grid_function($atts) {
     ob_start();
     ?>
     <?php 
     $cateId = $atts['cateid'];
     $args = [
          'cat' => $cateId,
          'post_type' => 'post',
          'order' => 'DESC',
          'orderby' => 'date',
          'posts_per_page' => 3,
     ];
     $query = new \WP_Query( $args );
     ?>
     <?php if ($query->have_posts()): ?>
     
     <?php $i = 0; ?>
     <?php while ( $query->have_posts() ) : $query->the_post(); ?>
          <?php 
           $thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large', true );
          if ($thumbnail_url)
               $thumbnail_url = $thumbnail_url[0];
          else
             $thumbnail_url = '';  
          ?>
          <?php if ($i == 0) echo '<div class="custom-blog-row">'; ?>
          <div class="custom-col" style="background-image:url(<?php echo $thumbnail_url ?>)">
               <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                    <div class="desc-wrapper">
                         <h3><?php the_title() ?></h3>
                         <span class="post-date"><?php echo get_the_date() ?> / <?php echo get_the_author() ?></span>
                    </div>
               </a>
          </div>
          <?php $query->reset_postdata(); ?>
          <?php $i++; ?>
          <?php if ($i == 3 || $i == $query->post_count) echo '</div>'; ?> 
     <?php endwhile; ?>

     <a href="<?php echo get_category_link($cateId) ?>" title="<?php echo get_cat_name($cateId); ?>" class="view-more"><?php echo esc_html_e('See all news and articles', 'xstore' ) ?></a>
     <?php endif; ?>
     <?php echo ob_get_clean();
}