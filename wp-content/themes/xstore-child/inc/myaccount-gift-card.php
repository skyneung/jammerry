<?php
function custom_add_giftcard_endpoint() {
   add_rewrite_endpoint( 'giftcard', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'custom_add_giftcard_endpoint' );
 
 
/**
* 2. Add new query var
*/
function custom_giftcard_query_vars( $vars ) {
   $vars[] = 'giftcard';
   return $vars;
}
 
add_filter( 'woocommerce_get_query_vars', 'custom_giftcard_query_vars', 0 );
 
 
/**
* 3. Insert the new endpoint into the My Account menu
*/
function custom_add_giftcard_link_my_account( $items ) {
   $items['giftcard'] = __('ซื้อบัตรของขวัญ', 'xstore');
   return $items;
}
 
add_filter( 'woocommerce_account_menu_items', 'custom_add_giftcard_link_my_account' );
 
 
/**
* 4. Add content to the new endpoint
*/
function custom_giftcard_content() {
   $args = [
      'post_type' => 'product',
      'postname' => 'gift-card',
   ];
   $the_query = new WP_Query( $args );
   if ($the_query->have_posts()) {
     
      while ($the_query->have_posts()) {
         $the_query->the_post();
         $giftCardId = get_the_ID();
         break;
      }
   }
   $the_query->reset_postdata();
   $_product = new WC_Product( $giftCardId );
   $giftCardUrl = get_the_permalink($_product->ID);
   wp_redirect($giftCardUrl,302);
}

/**
* @important-note	"add_action" must follow 'woocommerce_account_{your-endpoint-slug}_endpoint' format
*/
add_action( 'woocommerce_account_giftcard_endpoint', 'custom_giftcard_content' );