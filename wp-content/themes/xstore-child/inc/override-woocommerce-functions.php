<?php
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	function woocommerce_get_product_thumbnail( $size = 'full', $deprecated1 = 0, $deprecated2 = 0 ) {
		global $product;
		return $product ? $product->get_image( 'full' ) : '';
	}
}