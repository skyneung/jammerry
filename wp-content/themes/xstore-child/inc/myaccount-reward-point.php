<?php
function custom_add_reward_point_endpoint() {
   add_rewrite_endpoint( 'reward-point', EP_ROOT | EP_PAGES );
}
 
add_action( 'init', 'custom_add_reward_point_endpoint' );
 
 
/**
* 2. Add new query var
*/
 
function custom_reward_point_query_vars( $vars ) {
   $vars[] = 'reward-point';
   return $vars;
}
 
add_filter( 'woocommerce_get_query_vars', 'custom_reward_point_query_vars', 0 );
 
 
/**
* 3. Insert the new endpoint into the My Account menu
*/
 
function custom_add_reward_point_link_my_account( $items ) {
   $items['reward-point'] = __('คะแนนสะสม', 'xstore');
   return $items;
}
 
add_filter( 'woocommerce_account_menu_items', 'custom_add_reward_point_link_my_account' );
 
 
/**
* 4. Add content to the new endpoint
*/
 
function custom_reward_point_content() {
   get_template_part( 'woocommerce/myaccount/custom-reward-point');
}

/**
* @important-note	"add_action" must follow 'woocommerce_account_{your-endpoint-slug}_endpoint' format
*/
add_action( 'woocommerce_account_reward-point_endpoint', 'custom_reward_point_content' );