$(document).ready(function () {
    //cart
    $('#header .et_b_header-cart').on('click', function (e) {
        // Cart button won't be click because we need to display cart popup instead.
        if ($(e.target).parents('.et_b-icon').hasClass('et_b-icon')) {
            e.preventDefault();
            if ($('#header .et_b_header-cart .et-mini-content').hasClass('visible')) {
                $('#header .et_b_header-cart .et-mini-content').removeClass('visible');
                $('body').removeClass('minicart-open');
            } else {
                $('#header .et_b_header-cart .et-mini-content').addClass('visible');
                $('body').addClass('minicart-open');
            }
        }
    });

    $('#header .et_b_header-cart .et-mini-content').on('mouseleave', function () {
        $('#header .et_b_header-cart .et-mini-content').removeClass('visible');
        $('body').removeClass('minicart-open');
    });

    //search
    $('#header .et_b_header-search').on('click', function (e) {
        // Cart button won't be click because we need to display cart popup instead.
        if ($(e.target).hasClass('et_b_search-icon')) {
            e.preventDefault();
            if ($('#header .et_b_header-search .et-mini-content').hasClass('visible')) {
                $('#header .et_b_header-search .et-mini-content').removeClass('visible');
            } else {
                $('#header .et_b_header-search .et-mini-content').addClass('visible');
            }
        }
    });

    //clean all field of contactus form when user click button
    $('.clean-field-btn').on('click', function (e) {
        e.preventDefault();
        $('.wpcf7-form input, .wpcf7-form textarea').val('');
    });
    changeColumnPlist();
    $('.bnt-column').click(function () {
        $('.bnt-column').removeClass('active');
        $(this).addClass('active');
        changeColumnPlist();
    });

    $('.cart-bnt-column').click(function () {
        $('.cart-bnt-column').removeClass('active');
        $(this).addClass('active');
        changeColumnCart();
    });

    createModal();

    $(".et_b_header-cart .et-mini-content").append("<span class='close'></span>");
    $(".et_b_header-cart .et-mini-content .close").click(function () {
        console.log('clikc');
        $(".et_b_header-cart .et-mini-content").removeClass("visible");
    });

    $('.title-myaccount').click(function(){
        $('.woocommerce-MyAccount-navigation').toggle('slow');
    });

    mobileFIlterToggle();

    var screenWidth = $(window).width();
    if (screenWidth < 1280) {

    } else if (screenWidth < 820) {

    } else if (screenWidth < 768) {
    } else if (screenWidth < 575) {

    } else if (screenWidth <= 320) {

    } else {

    }

    $(window).on('resize', function () {
        var win = $(this);
        if (win.width() < 1280) {

        } else if (win.width() < 820) {

        } else if (win.width() < 768) {
        } else if (win.width() < 575) {

        } else if (win.width() <= 320) {

        } else {

        }
    });
});

function mobileFIlterToggle(){
    $('body').on("click",".shop-filters .sidebar-widget .widget-title", function(){
        if($(this).data('toggle')=="open"){
            $(this).data('toggle', 'close');
            $(this).removeClass('open');
            $(this).next().slideUp('fast');
        }else{
            $(this).data('toggle', 'open');
            $(this).addClass('open');
            $(this).next().slideDown('fast');
        }
    });

    $('body').on("click",".sidebar-filter-title", function(){
        $('.open-filters-btn a').trigger('click');
    });
    
}

function changeColumnPlist() {
    let plistColumn = $('.bnt-column.active').data('plist');
    if (typeof plistColumn != 'undefined') {
        if (plistColumn == 'two') {
            $('.products-grid').addClass('plist-two');
            $('.products-grid').removeClass('plist-one');
        } else {
            $('.products-grid').addClass('plist-one');
            $('.products-grid').removeClass('plist-two');
        }
        //console.log(plistColumn);
    }
}

function changeColumnCart(){
    let cartColumn = $('.cart-bnt-column.active').data('cartrow');
    if (typeof cartColumn != 'undefined') {
        if (cartColumn == 'one') {
            $('.cart-order-main-content').addClass('cart-one');
            $('.cart-order-main-content').removeClass('cart-two');
        } else {
            $('.cart-order-main-content').addClass('cart-two');
            $('.cart-order-main-content').removeClass('cart-one');
        }
        //console.log(plistColumn);
    }
}

function createModal(content = '') {
    $("body").append('<div class="modal"><div class="modal-content"><span class="close">&times;</span><div class="modal-detail">' + content + '</div></div></div>');

    // Get the modal
    var modal = $(".modal");

    // Get the <span> element that closes the modal
    var eleClose = $(".modal .close");

    // When the user clicks on <span> (x), close the modal
    eleClose.click(function () {
        modal.css('display', 'none');
    });
}

function popupClose() {
    $(".modal").css('display', 'none');
}

function popupModal(content = '', open = false) {
    $(".modal-detail").html(content);
    // When the user clicks the button, open the modal 
    if (open == true) {
        $(".modal").css('display', 'block');
    }
}