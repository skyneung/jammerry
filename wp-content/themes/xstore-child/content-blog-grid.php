<?php
global $et_loop;

if( empty( $et_loop['columns'] ) ) {
    $et_loop['columns'] = etheme_get_option('blog_columns');
}

if( empty( $et_loop['slider'] ) ) {
    $et_loop['slider'] = false;
}

if( empty( $et_loop['loop'] ) ) {
    $et_loop['loop'] = 0;
}

$options = array();

$options['layout'] = etheme_get_option('blog_layout');

if ( is_single() && $options['layout'] == 'timeline2' ) {
    $et_loop['slide_view'] = $options['layout'];
}

if( ! empty( $et_loop['blog_layout'] ) ) {
    $options['layout'] = $et_loop['blog_layout'];
}

$options['size'] = etheme_get_option( 'blog_images_size' );
$options['hide_img'] = false;

if( ! empty( $et_loop['size'] ) ) {
    $options['size'] = $et_loop['size'];
}

if( ! empty( $et_loop['hide_img'] ) ) {
    $options['hide_img'] = $et_loop['hide_img'];
}

$options['excerpt_length'] = etheme_get_option('excerpt_length');

$et_loop['loop']++;

$options['postClass']      = etheme_post_class( $options['layout'] );
// get permalink before content because if content has products then link is bloken
$options['the_permalink'] = get_the_permalink();



global $cm_blog_loop;
$cm_blog_loop['loop']++;
?>
<article class="custom-col <?php echo ($cm_blog_loop['loop'] > $cm_blog_loop['item_per_page']) ? 'hidden' : '' ?>" style="background-image:url(<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full') ?>)" id="post-<?php the_ID(); ?>" >
    <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
        <div class="desc-wrapper">
            
            <h3><?php the_title() ?></h3>
            <span class="post-date"><?php echo get_the_date() ?> / <?php echo get_the_author() ?></span>
        </div>
    </a>
</article>
<?php unset($options); ?>