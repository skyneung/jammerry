<section class="footer-newsletter">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <h4><?php echo __( 'Join our newsletter', 'xstore' ) ?></h4>
                <h3><?php echo __( 'Get 10% discount', 'xstore' ) ?></h3>
                <div class="mailchimp-form-wrapper"><?php echo do_shortcode('[mc4wp_form id="128"]') ?></div>
            </div>
        </div>
    </div>
</section>