<?php
defined( 'ABSPATH' ) || exit;
global $post;
$heading = apply_filters( 'woocommerce_product_description_heading', __( 'Shipping & delivery', 'woocommerce' ) );
?>

<?php if ( $heading ) : ?>
	<h2><?php echo esc_html(    $heading ); ?></h2>
<?php endif; ?>
<?php echo do_shortcode(etheme_get_custom_field('custom_tab1')); ?>