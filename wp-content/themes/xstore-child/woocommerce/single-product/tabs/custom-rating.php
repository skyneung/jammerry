<?php

global $post;

$heading = apply_filters( 'woocommerce_product_description_heading', __( 'Rating', 'woocommerce' ) );

?>

<?php if ( $heading ) : ?>
	<h2><?php echo esc_html( $heading ); ?></h2>
<?php endif; ?>

<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

global $product;

if ( ! wc_review_ratings_enabled() ) {
	return;
}



$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();


if ( $rating_count > 0 ) : ?>
	<?php foreach ($product->get_rating_counts() as $average => $rating_count): ?>
	<?php $percent = $average * 20; ?>
	<div class="rating-wrapper">
		<div class="star-icon">
			<?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>
		</div>
		<div class="process-box">
			<div class="progress">
				<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent ?>%">
				</div>
			</div>
		</div>
		<div class="rating-count"><?php echo $rating_count ?> คน</div>
	</div>
	<?php endforeach; ?>


<?php endif; ?>



