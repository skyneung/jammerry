<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.4
 */

defined( 'ABSPATH' ) || exit;

if ( ! wc_coupons_enabled() ) { // @codingStandardsIgnoreLine.
	return;
}
?>
<form class="custom-form-coupon">
    <div class="form-group">
        <input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Enter your coupon code here', 'woocommerce' ); ?>" id="custom-coupon_code" value="" />
    </div>
    <div class="responseMsg"></div>
    <button class="button" id="submitFormCoupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_html_e( 'Apply coupon', 'woocommerce' ); ?></button>
	<div class="clear"></div>
</form>

<script type="text/javascript">
$(document).ready(function(){
    $('body').on('click', '#submitFormCoupon', function(e){
        e.preventDefault();
        $.ajax({
            url: '<?php echo site_url() ?>?wc-ajax=apply_coupon',
            type: 'POST',
            data: {
                'security': wc_checkout_params.apply_coupon_nonce,
                'coupon_code': $('#custom-coupon_code').val(),
            },
            success: function(data){
               if (typeof data != undefined) {
                   $('.responseMsg').html(data);
                   //$('.errorBox').html(data.replace(/(<([^>]+)>)/gi, ""));
                   $( 'body' ).trigger( 'update_checkout' );
               }
               
            }
        });
    });
});
</script>
