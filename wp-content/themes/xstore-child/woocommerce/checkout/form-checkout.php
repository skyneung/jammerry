<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<h2 class="checkout-title"><?php echo esc_html_e('Payment', 'xstore') ?></h2>
<?php if ( etheme_get_option('cart_special_breadcrumbs') ) : ?>
<div class="cart-checkout-nav">
<a href="<?php echo get_site_url(); ?>" class="active"> <?php esc_html_e('Home', 'xstore'); ?></a>
<span class="delimeter"> <?php echo etheme_get_cart_sep(); ?></span>
<a href="<?php echo wc_get_checkout_url(); ?>" class="active no-click"> <?php esc_html_e('Checkout', 'xstore'); ?></a>
</div>

<?php endif; ?>

<?php wc_print_notices(); ?>

<div class="before-checkout-form">
	<p class="notice-fill-billing-info"><?php echo esc_html_e('Fill out your information in the form below. For payment and shipping.', 'woocommerce') ?></p>
	<?php
		do_action( 'woocommerce_before_checkout_form', $checkout );
	?>
</div>
<?php

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'xstore' ) ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', wc_get_checkout_url() ); ?>

<div class="checkout-wrapper">
<div class="login-wrapper">
<?php woocommerce_login_form(
	array(
		'message'  => esc_html__( 'If you have shopped with us before, please enter your details below. If you are a new customer, please proceed to the Billing section.', 'woocommerce' ),
		'redirect' => wc_get_checkout_url(),
		'hidden'   => true,
	)
); ?>
</div>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
	
	<div class="row">
		<div class="col-md-7 clearfix">
			<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>
		
				<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
		
				<div id="customer_details">
		
					<div class="col-1">
		
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
		
					</div>
		
					<div class="col-2">
		
						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
		
					</div>
		
				</div>
		
				<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
		
			<?php endif; ?>
		</div>
		
		<div class="col-md-5 cart-order-details">
			<div class="order-review">
				
				<h3 class="step-title coupon-title" data-accordionid="#form-coupon-and-giftcard"><span><?php esc_html_e( 'Coupon Code', 'woocommerce' ); ?></span></h3>
				<div class="coupon-wrapper" style="display:none;" id="form-coupon-and-giftcard"><?php do_action( 'cmprodev_checkout_above_order_review' ); ?></div>
				
				<h3 class="step-title order-review-title open" data-accordionid="#order_review"><span><?php esc_html_e( 'Your order', 'xstore' ); ?></span></h3>
				<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				</div>

				<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</div>
		</div>
		<script type="text/javascript">
		$(document).ready(function(){
			$('body').on('click', '.order-review .step-title', function(e){
				e.preventDefault();
				var thisObj = $(this);
				var accordionid = $(this).data('accordionid');
				if (!$(accordionid).is(':visible')) {
					$(accordionid).slideDown();
					thisObj.addClass('open');
				} else {
					$(accordionid).slideUp();
					thisObj.removeClass('open');
				}
			});

			$('body').on('click', '.woocommerce-form-login-toggle .showlogin', function(e){
				var loginForm = $('.woocommerce-form.woocommerce-form-login');
				var billingForm = $('#customer_details');
				if (loginForm.is(':visible')) {
					billingForm.removeClass('element-invisible');
				} else {
					billingForm.addClass('element-invisible');
				}
			})

			$('body').on('click', '#hideLoginForm', function(e){
				e.preventDefault();
				var loginForm = $('.woocommerce-form.woocommerce-form-login');
				var billingForm = $('#customer_details');
				if (loginForm.is(':visible')) {
					loginForm.slideUp();
					billingForm.removeClass('element-invisible');
				}
			})
		});
		</script>
	</div>

</form>
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>