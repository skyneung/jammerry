<h2 class="myaccount-title"><?php echo esc_html_e('คะแนนสะสม (Reward Points)','xstore') ?></h2>
<div class="reward-point-wrapper">
   <div class="reward-point-box">
      <strong class="reward-point-title"><?php echo esc_html_e('คะแนนสะสมของคุณ', 'xstore') ?></strong>
      <div class="reward-point-in-circle"><span class="point"><?php echo (int)do_shortcode('[rs_my_reward_points]') ?></span><span class="label"><?php echo esc_html_e('คะแนน', 'xstore') ?></span></div>
   </div>
   <div class="reward-point-info hidden">
      <div class="">
         <div class="lable"><?php echo esc_html_e('คะแนนสะสม ถึงวันนี้', 'xstore') ?></div>
         <div class="info"><?php echo date('Y-m-d H:i:s') ?></div>
      </div>
      <div class="">
      <div class="lable"><?php echo esc_html_e('คะแนนสะสม จะหมดอายุภายในวันที่', 'xstore') ?></div>
      <div class="info"><?php echo date('Y-m-d H:i:s') ?></div>
      </div>
   </div>
   <div class="reward-point-note">
      <p><?php echo esc_html_e('ทุกการซื้อครบ 50 บาท รับ 1 คะแนน / ทุก 10 คะแนน สามารถนำเป็นลดส่วนในการซื้อสินค้าได้ 10 บาท การใช้คะแนน ใช้ในขั้นตอนการชำระเงิน ด้วยวิธีจ่ายด้วยคะแนนสะสม', 'xstore') ?></p>
   </div>
</div>