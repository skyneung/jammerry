<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>
<div class="title-myaccount"><?php echo esc_html('บัญชีผู้ใช้งาน'); ?><svg xmlns="http://www.w3.org/2000/svg" width="20.033" height="13" viewBox="0 0 20.033 13">
  <g id="_120" data-name=" 120" transform="translate(-683 -702)">
    <path id="Path_36" data-name="Path 36" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
    <path id="Path_2236" data-name="Path 2236" d="M12.016,15,2,4.984,4.984,2l7.033,7.033L19.049,2l2.984,2.984Z" transform="translate(681 700)" fill="#fff"/>
  </g>
</svg>
</div>
<nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
