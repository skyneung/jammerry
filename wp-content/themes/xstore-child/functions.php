<?php
require get_stylesheet_directory() . '/inc/shortcode.php';
require get_stylesheet_directory() . '/inc/override-woocommerce-functions.php';
require get_stylesheet_directory().'/inc/myaccount-reward-point.php';
require get_stylesheet_directory().'/inc/myaccount-gift-card.php';
require get_stylesheet_directory().'/inc/myaccount-term-and-condition.php';

function debug($data) {
	echo '<pre>';
	var_dump($data);
	echo '</pre>';
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 1000);
function theme_enqueue_styles() {
	etheme_child_styles();
	wp_enqueue_style( 'mitr-font', 'https://fonts.googleapis.com/css2?family=Mitr:wght@200;300;400;500;600;700&display=swap', []);

     wp_enqueue_style( 'cmprodev-owl-carousel', get_stylesheet_directory_uri() . '/OwlCarousel/assets/owlcarousel/assets/owl.carousel.min.css' , []);
     wp_enqueue_style( 'cmprodev-owl-theme-default', get_stylesheet_directory_uri() . '/OwlCarousel/assets/owlcarousel/assets/owl.theme.default.min.css' , []);
}

add_action( 'wp_footer', 'addCustomCssToFooter');
function addCustomCssToFooter() {
	wp_enqueue_style( 'cmprodev-custom', get_stylesheet_directory_uri() . '/css/custom.css');
}

add_action( 'woocommerce_archive_description', 'woocommerce_breadcrumb', 10, 0 );
add_action( 'cmprodev_product_single_breadcumb', 'woocommerce_breadcrumb', 20, 0 );

/**
 * Remove default breadcumb becuase we would like to move breadcumb down product category title
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

add_action( 'cmprodevNewsletterFooter', 'cmprodevNewsletterFooter', 10 );
function cmprodevNewsletterFooter(){
    get_template_part( 'templates/footer/cmprodevnewsletterfooter');
}

add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
     $currencies['custom_thai_currency'] = __( 'Custom Thai Currency', 'woocommerce' );
     return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'custom_thai_currency': $currency_symbol = 'บาท'; break;
     }
     return $currency_symbol;
}

//add wishlist button to product single page
add_action('woocommerce_after_add_to_cart_quantity', 'add_wishlist_button', 10);
function add_wishlist_button() {
     echo do_shortcode('[yith_wcwl_add_to_wishlist]');
}

//Delete default product tab of theme and then we add it back after add to cart form
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action('custom_after_single_product_summary', 'woocommerce_output_product_data_tabs',10);

/** custom product tab on single product page */
add_filter( 'woocommerce_product_tabs', 'add_cmprodev_custom_product_tabs', 98 );
function add_cmprodev_custom_product_tabs( $tabs ) {
     //remove additional information tab
     unset($tabs['additional_information']);
     unset($tabs['reviews']);

     //add size guide tab
     $sizeGuideTab = [
          'title' => __('Size guide', 'woocommerce'),
          'priority' => 12,
          'callback' => 'woocommerce_cmprodev_sizeguide_tab',
     ];
    $tabs['woocommerce_cmprodev_sizeguide_tab'] = $sizeGuideTab;

     //add shipping tab
     //TODO:: we remove theme's default shipping tab and then we add custom shipping tab instead
     $sizeGuideTab = [
          'title' => __('Shipping & delivery', 'woocommerce'),
          'priority' => 13,
          'callback' => 'woocommerce_cmprodev_shipping_tab',
     ];
     $tabs['woocommerce_cmprodev_shipping_tab'] = $sizeGuideTab;

     //add payment tab
     $sizeGuideTab = [
          'title' => __('Payment', 'woocommerce'),
          'priority' => 14,
          'callback' => 'woocommerce_cmprodev_payment_tab',
     ];
     $tabs['woocommerce_cmprodev_payment_tab'] = $sizeGuideTab;

     //add Store policy  tab
     $sizeGuideTab = [
          'title' => __('Store Policies', 'woocommerce'),
          'priority' => 15,
          'callback' => 'woocommerce_cmprodev_store_policy_tab',
     ];
     $tabs['woocommerce_cmprodev_store_policy_tab'] = $sizeGuideTab;

     //add rating tab
     $sizeGuideTab = [
          'title' => __('Rating', 'woocommerce'),
          'priority' => 16,
          'callback' => 'woocommerce_cmprodev_rating_tab',
     ];
     $tabs['woocommerce_cmprodev_rating_tab'] = $sizeGuideTab;

     //add reviews tab
     $sizeGuideTab = [
          'title' => __('Reviews', 'woocommerce'),
          'priority' => 17,
          'callback' => 'comments_template',
     ];
     $tabs['comments_template'] = $sizeGuideTab;

     //add Share  tab
     $sizeGuideTab = [
          'title' => __('Share', 'woocommerce'),
          'priority' => 18,
          'callback' => 'woocommerce_cmprodev_share_tab',
     ];
     $tabs['woocommerce_cmprodev_share_tab'] = $sizeGuideTab;
     return $tabs;
}

if ( ! function_exists( 'woocommerce_cmprodev_sizeguide_tab' ) ) {
	function woocommerce_cmprodev_sizeguide_tab() {
		wc_get_template( 'single-product/tabs/sizeguide.php' );
	}
}

if ( ! function_exists( 'woocommerce_cmprodev_rating_tab' ) ) {
	function woocommerce_cmprodev_rating_tab() {
		wc_get_template( 'single-product/tabs/custom-rating.php' );
	}
}

if ( ! function_exists( 'woocommerce_cmprodev_payment_tab' ) ) {
	function woocommerce_cmprodev_payment_tab() {
		wc_get_template( 'single-product/tabs/custom-payment.php' );
	}
}

if ( ! function_exists( 'woocommerce_cmprodev_store_policy_tab' ) ) {
	function woocommerce_cmprodev_store_policy_tab() {
		wc_get_template( 'single-product/tabs/custom-store-policy.php' );
	}
}

if ( ! function_exists( 'woocommerce_cmprodev_share_tab' ) ) {
	function woocommerce_cmprodev_share_tab() {
		wc_get_template( 'single-product/tabs/custom-share.php' );
	}
}

if ( ! function_exists( 'woocommerce_cmprodev_shipping_tab' ) ) {
	function woocommerce_cmprodev_shipping_tab() {
		wc_get_template( 'single-product/tabs/custom-shipping.php' );
	}
}
/** End */

add_theme_support( 'post-thumbnails' );
add_image_size( 'related-product-size', 540, 600, ['center', 'center'] );

add_filter( 'image_size_names_choose', 'register_related_product_size' );
function register_related_product_size( $sizes ) {
    return array_merge( $sizes, array(
        'related-product-size' => __( 'Related products size' ),
    ) );
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'cmprodev_checkout_above_order_review', 'custom_checkout_coupon_form', 10 );

if ( ! function_exists( 'custom_checkout_coupon_form' ) ) {
	function custom_checkout_coupon_form() {
		wc_get_template(
			'checkout/custom-form-coupon.php',
			array(
				'checkout' => WC()->checkout(),
			)
		);
	}
}

function woocommerce_review_order_before_submit() {
     wp_enqueue_script( 'pw-gift-cards' );
     wc_get_template( 'checkout/custom-form-gift-card.php');
}
add_action( 'cmprodev_checkout_above_order_review', 'woocommerce_review_order_before_submit' );

add_action( 'woocommerce_login_form_end', 'add_none_acc_checkout', 20);
function add_none_acc_checkout() {
     ob_start();
     ?>
          <div class="none-acc-checkout-wrapper">
               <div class="none-acc-header"><span class="none-acc-icon"></span> <?php echo esc_html_e('Buy without a user account', 'xstore') ?></div>
               <p><?php echo esc_html_e('You can purchase in Non Account Login format, just need to fill out. Name and address Contact information For shipping.', 'xstore') ?></p>
               <div class="text-center">
                    <button id="hideLoginForm"><?php echo esc_html_e('Continue', 'xstore') ?></button>
               </div>
          </div>
     <?php
     echo ob_get_clean();
}

function get_continue_shopping_link() {
     return site_url().'/product-category/boy-collections/';
}

add_action('wp_enqueue_scripts', 'cmprodev_custom_footerjs',1000);
function cmprodev_custom_footerjs() {
     wp_enqueue_script( 'cmprodev_custom', get_stylesheet_directory_uri() . '/js/custom.js', array(), false, true );
     
     wp_enqueue_script( 'owlcarousel2', get_stylesheet_directory_uri() . '/OwlCarousel/assets/owlcarousel/owl.carousel.min.js', ['jquery'], '1', false );
}

/**
 * Update cart total in mobile menu when some product is added
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	ob_start();
	?>
	<a class="cart-customlocation" href="<?php echo esc_url(wc_get_checkout_url()); ?>"><span class="cart-icon"></span> <?php echo __('Your cart', 'xstore') ?> <span class="cart-total"><?php echo $woocommerce->cart->cart_contents_count ?></span></a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
}

add_action( 'woocommerce_proceed_to_checkout', 'add_coupon_form_to_sidebar_of_cartpage', 10 );

function add_coupon_form_to_sidebar_of_cartpage () {
     ob_start();
     ?>
     <?php if ( wc_coupons_enabled() ) : ?>
     <div>
          <form class="checkout_coupon" method="post">
          <span class="et-open to_open-coupon"><i class="et-icon et-coupon"></i><?php esc_html_e('Enter your promotional code', 'xstore'); ?></span>
               <div>

                    <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_html_e( 'Coupon code', 'xstore' ); ?>" />
                    <!-- <input type="submit" class="btn" name="apply_coupon" value="&#9166;" /> -->
                    <input type="submit" class="btn" name="apply_coupon" value="<?php esc_attr_e('OK', 'xstore'); ?>" />

                    <?php do_action('woocommerce_cart_coupon'); ?>

               </div>
          </form>
     </div>
     <?php endif; ?>
     <?php
     echo ob_get_clean();
}

function ignore_etheme_page_heading_action() {
     if (is_cart() || is_checkout()) {
          return true;
     } else {
          return false;
     }
}

if( !function_exists( 'cmprodev_owlcarousel2' ) ) {
	function cmprodev_owlcarousel2( $args ) {
          ob_start();
          global $owlCarouselLoop;
          $multislides = new WP_Query( $args );
          $owlCarouselLoop++;
          ?>
          <div class="owl-carousel product-owlcarousel" id="product-owlcarousel-<?php echo $owlCarouselLoop ?>">
          <?php
          while ( $multislides->have_posts() ) : $multislides->the_post(); ?>
              <?php global $product; ?>
              <?php if ( ! $product->is_visible() ) continue; ?>
               <div><?php wc_get_template_part( 'content', 'owlcarousel-product-slider' ) ?></div>
          <?php endwhile; ?>
          </div>
          <script type="text/javascript">
          $(document).ready(function(){
               $('#product-owlcarousel-<?php echo $owlCarouselLoop ?>').owlCarousel({
                    items: 3,
                    margin: 30,
                    responsive:{
                         0:{
                              items: 1
                         },
                         575:{
                              items: 2
                         },
                         768:{
                              items: 3
                         }
                    },
                    lazyLoad: false,
                    nav: true,
                    autoplay: true,
                    loop: true,
               });
          });
          </script>
          <?php echo ob_get_clean();
     }
}

if( !function_exists( 'cmprodev_swiper' ) ) {
	function cmprodev_swiper( $args ) {
          ob_start();
          global $cmprodevSwiperLoop;
          $multislides = new WP_Query( $args );
          $cmprodevSwiperLoop++;
          ?>
          <!-- Slider main container -->
          <div class="swiper-container product-swiper-<?php echo $cmprodevSwiperLoop ?>">
               <!-- Additional required wrapper -->
               <div class="swiper-wrapper">
               <?php
               while ( $multislides->have_posts() ) : $multislides->the_post(); ?>
               <?php global $product; ?>
               <?php if ( ! $product->is_visible() ) continue; ?>
                    <div class="swiper-slide"><?php wc_get_template_part( 'content', 'owlcarousel-product-slider' ) ?></div> 
               <?php endwhile; ?>
               </div>

               <!-- If we need navigation buttons -->
               <div class="swiper-button-prev"></div>
               <div class="swiper-button-next"></div>
          </div>

          <script type="text/javascript">
          const swiper<?php echo $cmprodevSwiperLoop ?> = new Swiper('.product-swiper-<?php echo $cmprodevSwiperLoop ?>', {
               slidesPerView: 3,
               spaceBetween: 30,
               centeredSlides: true,
               navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
               },
          });
          </script>
          <?php echo ob_get_clean();
     }
}

wp_deregister_script('wc-add-to-cart');
wp_register_script('wc-add-to-cart', get_bloginfo( 'stylesheet_directory' ). '/js/woo/add-to-cart.js' , array( 'jquery' ), WC_VERSION, TRUE);
wp_enqueue_script('wc-add-to-cart');

add_action('wp_head','woocommerce_js');

function woocommerce_js()
{ // break out of php 
?>
<script>
     var txtDeleteCart = '<?php echo __('คุณแน่ใจที่จะลบสินค้าออกจากตะกร้า ?'); ?>';
</script>
<?php }

add_filter( 'get_the_archive_title', function ($title) {    
     if ( is_category() ) {    
             $title = single_cat_title( '', false );    
         } elseif ( is_tag() ) {    
             $title = single_tag_title( '', false );    
         } elseif ( is_author() ) {    
             $title = '<span class="vcard">' . get_the_author() . '</span>' ;    
         } elseif ( is_tax() ) { //for custom post types
             $title = sprintf( __( '%1$s' ), single_term_title( '', false ) );
         } elseif (is_post_type_archive()) {
             $title = post_type_archive_title( '', false );
         }
     return $title;    
 });

 