<?php
/**
 * The main template file.
 *
 */

defined( 'ABSPATH' ) || exit( 'Direct script access denied.' );

get_header();


?>
<div class="page-title"><h1><?php echo esc_html_e('ข่าวและบทความ', 'xstore') ?></h1></div>
<?php

$l = etheme_page_config();

$content_layout = etheme_get_option('blog_layout');
$navigation_type = etheme_get_option( 'blog_navigation_type' );

$full_width = false;

if($content_layout == 'grid') {
	$full_width = etheme_get_option('blog_full_width');
	$content_layout = 'grid';
}

if ( $content_layout == 'grid2' ) {
	$full_width = etheme_get_option('blog_full_width');
	$content_layout = 'grid-2';
}

$class = 'hfeed';

$class .= ' et_blog-ajax';

$banner_pos = etheme_get_option( 'blog_page_banner_pos' );

if ( $content_layout == 'grid' || $content_layout == 'grid-2' ) {
	$class .= ' row';
	if ( etheme_get_option( 'blog_masonry' ) ) $class .= ' blog-masonry';
}

?>

<?php do_action( 'etheme_page_heading' ); ?>
<div class="actions-box">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3">
				<div class="sorting-post">
					<label for="shorting-post"><?php echo esc_html_e('Sorted by:', 'xstore') ?></label>
					<select name="shorting_post" id="shorting-post">
						<option value="newest"><?php echo esc_html_e('Newest') ?></option>
						<option value="oldest"><?php echo esc_html_e('Oldest') ?></option>
						<option value="mostview"><?php echo esc_html_e('Most visited') ?></option>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="choose-category">
					<?php
					$terms = get_terms( array( 
						'taxonomy' => 'category',
						'parent'   => 0
					) );
					if (is_array($terms)): ?>
						<?php foreach ($terms as $term): ?>
							<a href="<?php echo get_term_link($term->term_id) ?>"><?php echo $term->name ?></a>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3">
				<div class="search-box">
					<div class="search-button" id="search-button">
						<?php echo esc_html_e('Search for blog', 'xstore') ?>
						<span class="search-icon"></span>
					</div>
					<div id="search-input-box">
						<form method="POST" id="blog-search-form">
							<input type="text" id="search-blog-input" placeholder="">
							<input type="submit" class="btn btn-default" value="">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
/**
 * Custom Query
 */
$currentSlug = get_queried_object()->slug;
$args = [
	'posts_per_page' => -1,
	'post_type' => 'post',
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => $currentSlug,
		),
	),
];

/**
 * Sorting
 */
$sort = ($_GET['sort']) ? $_GET['sort'] : null;
if ($sort === 'newest') {
	$args['orderby'] = 'date';
	$args['order'] = 'DESC';
}
if ($sort === 'oldest') {
	$args['orderby'] = 'date';
	$args['order'] = 'ASC';
}
if ($sort === 'mostview') {
	$args['orderby'] = 'meta_value_num';
	$args['meta_key'] = '_et_views_count';
	$args['order'] = 'DESC';
}

if (isset($_GET['q']) && !empty($_GET['q'])) {
	$args['s'] = $_GET['q'];
	$keyword = ($_GET['q']) ? $_GET['q'] : '';
}
$the_query = new WP_Query( $args );
$numberOfContent = $the_query->found_posts;
global $cm_blog_loop;
$cm_blog_loop['loop'] = 0;
$cm_blog_loop['item_per_page'] = 9;
?>
<?php if (isset($_GET['q']) && !empty($_GET['q'])): ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-xs-12 col-md-12">
			<div class="search-result-count"><?php echo _e(sprintf('ผลการค้นหา "<span class="keyword">%s</span>" <span class="found-count">พบ : %d บทความ</span>', $keyword, $numberOfContent), 'xstore') ?></div>
		</div>
	</div>
</div>
<?php endif; ?>

<div class="content-page <?php echo ( ! $full_width ) ? 'container' : 'blog-full-width'; ?> sidebar-mobile-<?php etheme_option('blog_sidebar_for_mobile'); ?>">
	<div class="custom-blog-row">
		<?php if($the_query->have_posts()):
			while($the_query->have_posts()) : $the_query->the_post(); ?>
				<?php get_template_part('content', 'blog-grid'); ?>
			<?php endwhile; ?>
		<?php else: ?>

			<div class="col-md-12">

				<h2 class="not-found-post"><?php esc_html_e('ไม่พบเนื้อหาที่คุณค้นหา', 'xstore') ?></h2>
			</div>

		<?php endif; ?>
	</div>
</div>


<?php if ($numberOfContent > $cm_blog_loop['item_per_page']): ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 text-center">
			<button id="loadmorepost"><?php echo esc_html_e('โหลดเพิ่ม', 'xstore') ?></button>
		</div>
	</div>
</div>
<?php endif; ?>

<script type="text/javascript">
	document.getElementById('shorting-post').addEventListener('change', function(e){
		window.location.href = window.location.origin+window.location.pathname+'?sort='+e.target.value;
	});
	document.getElementById('search-button').addEventListener('click', function(e){
		var searchInputBox = document.getElementById('search-input-box');
		var reg = /active/;
		if (reg.test(searchInputBox.className)) {
			searchInputBox.classList.remove("active");
		} else {
			searchInputBox.classList.add("active");
		}
	});
	document.getElementById('blog-search-form').addEventListener('submit', function(e){
		e.preventDefault();
		var searchTxt = document.querySelector('#blog-search-form input[type="text"]').value;
		window.location.href = window.location.origin+window.location.pathname+'?q='+searchTxt;
	});
	document.getElementById('loadmorepost').addEventListener('click', function(e){
		const postItems = document.querySelectorAll('.custom-col.hidden');
		postItems.forEach(function(element,index){
			if (index < <?php echo $cm_blog_loop['item_per_page'] ?>) {
				element.classList.remove('hidden');
			}
		});
		if (document.querySelectorAll('.custom-col.hidden').length === 0) {
			e.target.classList.add('hidden');
		}
	});
</script>
<?php
get_footer();
?>