<?php

defined( 'ABSPATH' ) || exit( 'Direct script access denied.' );

get_header();

global $post;
$l = etheme_page_config();
$post_template  = get_query_var('et_post-template', 'default');
$post_format 	= get_post_format();
$post_large = ($post_template == 'large' || $post_template == 'large2') ? true : false;
?>
<div class="blog-single-post">
   <div class="page-title"><h2><?php echo esc_html_e('ข่าวและบทความ', 'xstore') ?></h2></div>
   <?php do_action( 'etheme_page_heading' ); ?>
   <div class="container-fluid">
      <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
               
               <div class="feature-image">
                  <img src="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full', true ); ?>" />
               </div>
               <h1 class="post-title"><?php the_title() ?></h1>
               <span class="post-date"><?php echo get_the_date() ?> / <?php echo get_the_author() ?></span>
            </div>
      </div>
   </div>
   <article>
      <?php the_content();?>
   </article>
   <div class="container-fluid">
      <?php if(etheme_get_option('post_share') && class_exists('ETC\App\Controllers\Shortcodes\Share')): ?>
         <div class="share-post">
            <?php echo ETC\App\Controllers\Shortcodes\Share::share_shortcode( array( 'title' => esc_html__('Share Post', 'xstore') ) ) ; ?>
         </div>
      <?php endif; ?>
   </div>
</div>
<?php
get_footer();
?>